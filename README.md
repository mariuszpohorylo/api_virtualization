# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###


### How do I get set up? ###

* Install java 8 or open java
Go to https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html or https://openjdk.java.net/
Install jdk 8 or open jdk

* Download WireMock
Download it manually from this link https://repo1.maven.org/maven2/com/github/tomakehurst/wiremock-jre8-standalone/2.26.3/wiremock-jre8-standalone-2.26.3.jar
or execute following scripts:
- if you have windows, open powershell and execute following command in main folder of repository: ./download_windows.ps1
- if you have linux open shell and execute following command in main folder of repository: ./download_unix.sh

* Configuration
Configuration of WireMock templates is described here https://blackswantechnologies.atlassian.net/wiki/spaces/QA/pages/1627586694/Wiremock+Configuration
* Executing WireMock
Run this command to execute WireMock: java -jar wiremock-standalone-2.26.3.jar --port 8888 --https-port 8889 --verbose --global-response-templating 
or execute follorwing scripts:
- ./run.ps1 in windows machine
- ./run.sh in linux machine


### Who do I talk to? ###

* Repo owner mariusz@blackswan-technologies.com