$url = "https://repo1.maven.org/maven2/com/github/tomakehurst/wiremock-jre8-standalone/2.26.3/wiremock-jre8-standalone-2.26.3.jar"
$output = "$PSScriptRoot\wiremock-jre8-standalone-2.26.3.jar"
$start_time = Get-Date

$FileName = "$PSScriptRoot\wiremock-jre8-standalone-2.26.3.jar"
if (Test-Path $FileName) 
{
  Remove-Item $FileName
}

$wc = New-Object System.Net.WebClient
$wc.DownloadFile($url, $output)
Write-Output "Time taken: $((Get-Date).Subtract($start_time).Seconds) second(s)"o
